const sleep = (ms) => new Promise((r) => setTimeout(r, ms));
const random = (min, max) => {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
};

const saver = async () => {
	while (true) {
		for (const char of "паша кодит ".split("")) {
			process.stdout.write(char);
      // await sleep(random(50, 400));
      await sleep(random(20, 80))
		}
	}
};

saver();
